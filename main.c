#include <stdio.h>
#include <string.h>

#include "cmd_handle.h"

#define SZ_CMD 64


int main(void)
{
	char command[SZ_CMD] = {0};
	
	for(;;){
		
		printf("TinyShell > ");
		fgets(command,SZ_CMD,stdin); //"ls -l"  ===> "ls -l\n"

		command[strlen(command) - 1] = '\0';
		
		// "quit" : 退出标识
		if (strncmp(command,"quit",4) == 0 ){
			printf("GoodBye\n");
			break;
		}

		cmd_execute(command); // cmd_handle 模块中
	}
	return 0;
}
